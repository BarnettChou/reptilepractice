from bs4 import BeautifulSoup
import requests

#古力穀粒 關鍵字搜尋
goolegoole_url = 'http://www.goolegoole.com.tw/product.php'

payload = {'_path': 'search_product', 'search': '茶','search_submit':''}
r = requests.get(goolegoole_url, params = payload)
# print(r.url)
if r.status_code == requests.codes['ok']:
    # 以 BeautifulSoup 解析 HTML 原始碼
    soup = BeautifulSoup(r.text,'html.parser')
    # print(soup)
    stories = soup.find_all("div",class_="pure-u-md-3-4")
    # print(stories)
    # print(soup.prettify())
    for res in stories:
        content = res.find("h2",class_="pure-u-1-1").text
        print(content)
        for single in res.find_all("div",class_="pure-u-1"):
            print("---各項商品---")
            print(single.find("a")['href']) #網站連結
            print(single.find("img")['src']) #圖片路徑
            print(single.find("div",class_="product_title").string) #商品
            print(single.find("div",class_="hot_product_producer").string) #賣家
            print(single.find("div",class_="hot_product_preorder").string) #價格
            print(single.find("div",class_="hot_product_discount").string) #特價