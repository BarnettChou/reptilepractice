from bs4 import BeautifulSoup
import requests

# 下載 古力穀粒 首頁內容 大綱搜尋
r = requests.get('http://www.goolegoole.com.tw/')

if r.status_code == requests.codes['ok']:
    soup = BeautifulSoup(r.text,'html.parser')
    stories = soup.find_all("div",class_="product")

    for da in stories:
        content = da.find("div",class_="index_product_title").text
        print("---目錄---")
        print(content)

        for ca in da.find_all("div",class_="pure-u-md-1-4"):
            print("---各項商品---")
            print(ca.find("a")['href']) #網站連結
            print(ca.find("img")['src']) #圖片路徑
            print(ca.find("div",class_="product_title").string) #商品
            print(ca.find("div",class_="hot_product_producer").string) #賣家
            print(ca.find("div",class_="hot_product_price").string) #價錢


